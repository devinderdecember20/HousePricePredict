Create Project Directory

Create virtual envirement with name .myvenv  -> . dot makes file hidden

```python -m venv .myvenv```

Travel down to the activate file and activate the virtual envirement

```.\.myvenv\Scripts\Activate.ps1```

After activating the enviroment install require packages

```pip install flask```

Create starter file index.py and start the project

```python index.py```

After completing the work stop the server and deactivate the envirement

Just write deactivate to deactivate from any location.
___________________________________________________________________

For Remote repositories ->
	Create .gitignore file	
	Create requirement.txt file containing all the name of installed packages
	
	pip list >requirements.txt
	
___________________________________________________________________
	
For Other Developers ..

After cloning the project run below command after creating the envirement to install the required packages with specific versions

```pip install -r requirements.txt```

______________________________

